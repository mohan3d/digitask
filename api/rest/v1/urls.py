from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from .views import FileCreate

urlpatterns = [
    path('login/', obtain_auth_token, name='api-login'),
    path('files/', FileCreate.as_view(), name='api-files-create'),
]
