from rest_framework import generics

from core.models import File
from .serializers import FileSerializer


class FileCreate(generics.CreateAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
