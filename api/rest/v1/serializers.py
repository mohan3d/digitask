from rest_framework import serializers

from core.models import File


class FileSerializer(serializers.ModelSerializer):
    """Serializes File object into different formats (json and xml) and vice versa."""

    class Meta:
        model = File
        fields = ('id', 'file')
