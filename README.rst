Digitask
========

Digizilla task, A `django`_ application consists of login-form, upload-form and API written in `python`_ 3.6.


Install Dependencies
--------------------

.. code-block:: bash

    $ pip install -r requirements.txt


Deployment
----------

.. code:: bash

    $ python manage.py migrate

**Note**: user must be created in order to test the task, the easiest way to do so is to
create super user.

.. code:: bash

    $ python manage.py createsuperuser


Development server
------------------

.. code:: bash

    $ python manage.py runserver


Project structure
-----------------
::

    .
    ├── api                             # api application contains everything related to api
    │   └── rest                        # all restful APIs versions will live here
    │       └── v1                      # api version 1
    │           ├── serializers.py      # contains classes that serialize objects to json/xml and vice versa
    │           ├── urls.py             # api version 1 exposed urls
    │           └── views.py            # views to process requests
    │
    ├── core                            # contains models and related logic.
    │   ├── migrations                  # models migrations related to all models in core app
    │   ├── static                      # static files (css, js)
    │   ├── templates                   # core app templates
    │   ├── admin.py                    # admin customization
    │   ├── apps.py                     # entry point for django project (used in core.settings)
    │   ├── auth.py                     # custom auth backends
    │   ├── forms.py                    # upload form
    │   ├── models.py                   # file model
    │   ├── urls.py                     # core application exposed urls
    │   ├── utils.py                    # utility functions (reverse)
    │   └── views.py                    # core application views
    │
    ├── digitask                        # root application.
    │   ├── settings.py                 # contains settings and configurations for all applications
    │   ├── urls.py                     # public accessible urls
    │   └── wsgi.py                     # entry point for wsgi
    │
    ├── media                           # uploaded files storage
    │
    ├── requirements.txt                # project dependencies
    ├── manage.py                       # entry point for django cli commands
    └── README.rst

.. _python: https://www.python.org/
.. _django: https://www.djangoproject.com/
