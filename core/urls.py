from django.urls import path

from . import views

urlpatterns = [
    path('', views.Home.as_view(), name='home'),
    path('profile/', views.Profile.as_view(), name='profile'),
    path('success/', views.Success.as_view(), name='success'),

    path('login/', views.Login.as_view(), name='login'),
    path('logout/', views.Logout.as_view(), name='logout'),

    path('reset/', views.PasswordReset.as_view(), name='password_reset'),
    path('reset/done/', views.PasswordResetDone.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/ ', views.PasswordResetConfirm.as_view(), name='password_reset_confirm'),

    path('upload/', views.UploadFile.as_view(), name='upload'),
]
