from django.utils.http import urlencode
from django import urls


def reverse(*args, **kwargs):
    """Lightweight wrapper of django built-in reverse with support for get query params."""
    get = kwargs.pop('get', {})
    url = urls.reverse(*args, **kwargs)

    if get:
        url += '?' + urlencode(get)

    return url
