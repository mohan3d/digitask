from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView, PasswordResetView, PasswordResetConfirmView, \
    PasswordResetDoneView
from django.urls import reverse_lazy
from django.views import generic

from .forms import UploadFileForm
from .utils import reverse


class Home(generic.TemplateView):
    template_name = 'core/home.html'


class Success(generic.TemplateView):
    template_name = 'core/success.html'


class Profile(LoginRequiredMixin,
              generic.TemplateView):
    template_name = 'core/profile.html'


class Login(LoginView):
    template_name = 'core/login.html'

    def get_success_url(self):
        return reverse('success', get={'next': reverse('profile')})


class Logout(LogoutView):
    next_page = reverse_lazy('home')


class PasswordReset(PasswordResetView):
    template_name = 'core/reset.html'


class PasswordResetDone(PasswordResetDoneView):
    template_name = 'core/reset_done.html'


class PasswordResetConfirm(PasswordResetConfirmView):
    template_name = 'core/reset_confirm.html'
    success_url = reverse_lazy('login')


class UploadFile(LoginRequiredMixin,
                 generic.FormView):
    form_class = UploadFileForm
    template_name = 'core/upload.html'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        form.instance.owner = self.request.user
        form.instance.save()
        return super().form_valid(form)
