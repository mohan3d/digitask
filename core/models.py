from django.conf import settings
from django.db import models


class File(models.Model):
    """Represents file entry related to a user."""
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              on_delete=models.CASCADE)
    file = models.FileField()

    def __str__(self):
        return self.file.name
