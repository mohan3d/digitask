function redirect() {
    const urlParams = new URLSearchParams(window.location.search);
    const next = urlParams.get('next');
    window.location.href = next !== null ? next : '/';
}

setTimeout(redirect, 3000);